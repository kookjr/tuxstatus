/*
 * tuxstatus -- Display status of tuxcast podcast downloads.
 * 
 * Copyright (C) 2009 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of tuxstatus.
 *
 * tuxstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tuxstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tuxstatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <iostream>

#include "RegEx.h"

RegEx::RegEx(std::string regex_str) {
    initialize(regex_str);
}

RegEx::RegEx(const RegEx& regex) {
    initialize(regex.pattern);
}

RegEx::~RegEx(void) {
    if (errbuf != NULL) delete errbuf;
    regfree(&regbuf);
}

RegEx& RegEx::operator=(const RegEx& regex) {
    initialize(regex.pattern);
}

bool RegEx::match(std::string str) {
    search_str = str;
    int mtch = regexec(&regbuf, search_str.c_str(), NUM_MATCHES, matches, 0);
    matched_state = (mtch == 0);
    return matched_state;
}

std::string RegEx::stringMatch(void) throw (RegExException) {
    if (! matched_state) {
        throw RegExException("no match reported, cannot get match results");
        //std::cout << "no match reported, cannot get match results"
        //          << std::endl; return "";
    }
    return std::string(search_str.c_str(), matches[0].rm_so,
                       (std::string::size_type )(matches[0].rm_eo - matches[0].rm_so));
}

std::string RegEx::subStringMatch(int which) throw (RegExException) {
    if (which < 1 || which >= NUM_MATCHES || ! matched_state) {
        throw ("no match reported, cannot get match sub results");
        //std::cout << "no match reported, cannot get match sub results"
        //          << std::endl; return "";
    }
    return std::string(search_str.c_str(), matches[which].rm_so,
                       (std::string::size_type )(matches[which].rm_eo -
                                                 matches[which].rm_so));
}

void RegEx::initialize(std::string regex_str) throw (RegExException) {
    errbuf  = new char[128];
    pattern = regex_str;
    matched_state = false;
    search_str = "";

    // compile pattern
    int err = regcomp(&regbuf, pattern.c_str(), REG_EXTENDED);
    if (err != 0) {
        regerror(err, &regbuf, errbuf, sizeof(errbuf));
        throw RegExException(errbuf);
        //std::cout << errbuf << std::endl; return;
    }
}
