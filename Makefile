# tuxstatus -- Display status of tuxcast podcast downloads.
# 
# Copyright (C) 2009 Mathew Cucuzella (kookjr@gmail.com)
#
# This file is part of tuxstatus.
#
# tuxstatus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tuxstatus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with tuxstatus.  If not, see <http://www.gnu.org/licenses/>.

PREFIX = /usr
APP    = tuxstatus

CPP       = g++
LDFLAGS   = -lastatus -lncurses

# TUX_DEBUG disables ncurses for debugging - it looks
# terrible but necessary for debugging
# Enable with "make DEBUG=1"
ifdef DEBUG
  CPPFLAGS  = -g -DTUX_DEBUG -I.
  LDFLAGS  += -g
else
  CPPFLAGS  = -O -I.
endif

SRC  = RegEx.cpp TuxStatus.cpp main.cpp
OBJ  = $(SRC:.cpp=.o)

all: $(APP)

$(APP): $(OBJ)
	g++ $(CFLAGS) -o $@ $(OBJ) $(LDFLAGS)

clean:
	rm -f $(OBJ)

clobber: clean
	rm -f $(APP)

install: all
	install -o root -g root -m 755 $(APP) $(PREFIX)/bin

uninstall:
	rm -f $(PREFIX)/bin/$(APP)
