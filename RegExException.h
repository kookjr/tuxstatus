/*
 * tuxstatus -- Display status of tuxcast podcast downloads.
 * 
 * Copyright (C) 2009 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of tuxstatus.
 *
 * tuxstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tuxstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tuxstatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REGEXEXCEPTION_H
#define REGEXEXCEPTION_H

#include <exception>

class RegExException : public std::exception {
public:
    RegExException(std::string reason) throw () : ex_reason(reason) {}
    virtual ~RegExException(void) throw () {}
    virtual const char* what(void) const throw () { return ex_reason.c_str(); }
private:
    std::string ex_reason;
};

#endif /* REGEXEXCEPTION_H */

/* Local Variables: */
/* mode: C++ */
/* End: */
