/*
 * tuxstatus -- Display status of tuxcast podcast downloads.
 * 
 * Copyright (C) 2009, 2010 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of tuxstatus.
 *
 * tuxstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tuxstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tuxstatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <map>
#include <iostream>
#include <sstream>
#include <string>

#include "RegEx.h"
#include "MultilineStatus.h"
#include "ProgressBarData.h"
#include "MarqueeData.h"
#include "TuxStatus.h"

#define IN_LINE_SZ 80
#define PB_LINES    5

int TuxStatus::get_free_line(std::map<std::string,int>& pmap) {
    // look through entries to see which lines are not in use
    std::map<int,int> in_use;
    std::map<std::string,int>::iterator iter;
    for (iter=pmap.begin(); iter != pmap.end(); ++iter) {
        if (iter->second != -1) {
            in_use[iter->second] = 1;
        }
    }
    std::map<int,int>::iterator uiter;
    for (int i=5; i < 5+PB_LINES; i++) {
        if ( (uiter=in_use.find(i)) == in_use.end() ) {
            return i;
        }
    }
    return -1;
}

TuxStatus::TuxStatus(int delay_ms) :
    delay(delay_ms),
    dl_count(1),
    print_summary(false),
    progress_map(),
    line_matches(),
    download_list(),
    status(NULL)
{
    status = new mls::MultilineStatus(4+PB_LINES
#ifdef TUX_DEBUG
                                      , 80, true
#endif
        );

    status->addMarqueeLine(1, "    Check");
    status->addMarqueeLine(2, " Download");
    status->addMarqueeLine(3, "     Skip");
    status->addMarqueeLine(4, " Queue", true);  // add a queue count to line (extra 3 chars)
    for (int i=5; i <= 4+PB_LINES; i++) {
        status->addProgressBarLine(i, "DL Thread");
    }

    line_matches.push_back(line_match_data("Progress ([[:digit:]]+) (.*)",
                                           &TuxStatus::proc_progress));
    line_matches.push_back(line_match_data("Checking feed \"(.*)\"",
                                           &TuxStatus::proc_check));
    line_matches.push_back(line_match_data("Downloading (.*)\\.\\.\\.",
                                           &TuxStatus::proc_download));
    line_matches.push_back(line_match_data("Excluding (.*)\\.\\.\\.",
                                           &TuxStatus::proc_skip));
    line_matches.push_back(line_match_data("Queuing (.*) for download",
                                           &TuxStatus::proc_queue));
}

TuxStatus::~TuxStatus(void) {
    // cleanup, and set screen back to normal (from curses mode)
    delete status;
    // print summary if required
    if (print_summary) {
        std::list<std::string>::iterator it;
        std::cout << "Summary:";
        for (it=download_list.begin() ; it != download_list.end(); it++) {
            std::cout << "  " << *it;
        }
        std::cout << std::endl;
    }

}

void TuxStatus::download_summary_opt(void) {
    print_summary = true;
}

void TuxStatus::run(void) {
    char* line = (char* )malloc(IN_LINE_SZ);
    size_t size = IN_LINE_SZ;
    size_t rsz;

    // read each input line, looking for regex matches to update display
    while ( (rsz=getline(&line, &size, stdin)) != -1 ) {
        line[strlen(line)-1] = 0;  // remove LF
        std::vector<struct line_match_data>::iterator it;
        for (it=line_matches.begin(); it != line_matches.end(); it++) {
            if (it->regex.match(line)) {
                (*this.*(it->action))(it->regex);
                break;
            }
        }
        if (delay) {
            usleep(delay * 1000);  // convert from micro to milliseconds
        }
    }

    if (line != NULL) free(line);
}

void TuxStatus::proc_progress(RegEx& reg) {
    // sometimes a feed will show %100 complete on its first progress, and then
    // go back to zero which causes us to release the status line (geekbrief,
    // possibly because of 3 redirects before hitting the download file). In
    // this case we need to reallocate a line
    std::map<std::string,int>::iterator iter;
    if ( (iter=progress_map.find(reg.subStringMatch(2))) == progress_map.end() ||
         iter->second == -1) {
        // reallocate a progress bar to this download file
        progress_map[reg.subStringMatch(2)] = get_free_line(progress_map);
    }

    int int_percent = atoi(reg.subStringMatch(1).c_str());
    status->setProgress(progress_map[reg.subStringMatch(2)],
                        (double )int_percent * 0.01,
                        reg.subStringMatch(2));
    if (int_percent == 100) {
        // deallocate the progress bar
        progress_map[reg.subStringMatch(2)] = -1;
    }
}

void TuxStatus::proc_check(RegEx& reg) {
    status->addMarqueeElement(1, reg.subStringMatch(1));
}

void TuxStatus::proc_download(RegEx& reg) {
    // now that it is being downloaded, remove from Queue marquee (if it was there)
    status->remMarqueeElement(4, reg.subStringMatch(1));
    // save away name for dl summary
    download_list.push_back(reg.subStringMatch(1));
    // add to download marquee
    std::ostringstream buf;
    buf << dl_count++;
    status->addMarqueeElement(2, buf.str() + "<" + reg.subStringMatch(1) + ">");
    // allocate a progress bar to this download file
    progress_map[reg.subStringMatch(1)] = get_free_line(progress_map);
}

void TuxStatus::proc_skip(RegEx& reg) {
    // now that it is being skipped, remove from Queue marquee (if it was there)
    status->remMarqueeElement(4, reg.subStringMatch(1));
    // add to the skip marquee
    status->addMarqueeElement(3, reg.subStringMatch(1));
}

void TuxStatus::proc_queue(RegEx& reg) {
    status->addMarqueeElement(4, reg.subStringMatch(1), 1);
}
