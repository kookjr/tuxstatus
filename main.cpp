/*
 * tuxstatus -- Display status of tuxcast podcast downloads.
 * 
 * Copyright (C) 2009, 2010 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of tuxstatus.
 *
 * tuxstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tuxstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tuxstatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <iostream>

#include "TuxStatus.h"

void usage() {
    std::cerr << "Usage: tuxstatus [options]" << std::endl;
    std::cerr << "    [-s|--simulate-delay <num_ms>]" << std::endl;
    std::cerr << "    [-d|--download-summary]" << std::endl;
}

static int delay = 0;
static bool download_summary = false;

bool process_options(int argc, char** argv) {
    int c;

    while (1) {
        static struct option long_options[] = {
            {"download-summary", 0, 0, 'd'},
            {"simulate-delay", 1, 0, 's'},
            {0, 0, 0, 0}
        };

        c = getopt_long(argc, argv, "ds:", long_options, 0);
        if (c == -1)
            break;

        switch (c) {
        case 'd':
            download_summary = true;
            break;

        case 's':
            delay = atoi(optarg);
            break;

        case '?':
        default:
            usage();
            return(false);
        }
    }

    if (optind < argc) {
        usage();
        return(false);
    }

    return true;
}

int main(int argc, char** argv) {
    if (! process_options(argc, argv)) {
        return 1;
    }

    TuxStatus ts(delay);
    if (download_summary)
        ts.download_summary_opt();
    ts.run();

    return 0;
}
