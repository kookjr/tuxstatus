# tuxstatus

tuxstatus -- Display status of tuxcast podcast downloads.

Version 0.3

*tuxstatus* reads the output to the *tuxcast* command (through a pipe)
and displays the podcasts download status. It does this in a
graphical way using ncurses. See the following sample screenshot.

![screen shot](https://gitlab.com/kookjr/tuxstatus/raw/master/images/tuxstatus_main.png)

The features include:

* nice graphical display
* ability to simulate *tuxcast* output from a file

## tuxcast

Version 0.4 or later of *tuxcast* is required. It can be found in
some package repositories or at:

* ``https://sourceforge.net/projects/tuxcast/develop``

# Installation

*tuxstatus* is a command line utility and must be installed from source.
It has the following build dependencies.

* ncruses library
* asciistatus library (``https://gitlab.com/kookjr/asciistatus``)

Install the ncurses package, and download/build/install asciistatus.

* Build the source.

    cd tuxstatus
    make
    sudo make install

# Sample Usage

    tuxcast -s 2>/dev/null | tuxstatus

Error messages are thrown away because they can mess up the graphical
output.

Sometimes print statements or the use of gdb(1) may be needed to debug
*tuxstatus*. This is not compatible with the mode ncurses leaves the
terminal in. For debugging, define DEBUG on the make command line
(make DEBUG=1) and rebuild. This will disable ncurses, and
just print every update with a return and allow debug or debugger
messages to be interspersed.

# Development
## Source Repository

*tuxstatus* is currently hosted at gitlab. The gitlab web page is
``https://gitlab.com/kookjr/tuxstatus``. The public git clone URL is

* ``https://gitlab.com/kookjr/tuxstatus.git``

## Running the Sample Simulation

This runs simulation mode and does not require the *tuxcast*
program. It uses some mocked up data in the same format
as *tuxcast* output.

    tuxstatus --simulate-delay 300 < sample_data

It can also run data captured from a real run of ``tuxcast -s``.
This can be captured during real download sessions with a command
like this.

    tuxcast 2>/dev/null | tee /tmp/sample_data_real | tuxstatus

## Full Sample Usage

    Usage: tuxstatus [options]
        [-s|--simulate-delay <num_ms>]
        [-d|--download-summary]

*simulate-delay* - set the number of milliseconds between input
lines read. This is only to simulate running without tuxcast.

*download-summary* - Some terminals reset the screen to the contents
that existed before curses was setup, in effect clearling the results
of tuxstatus. This option can be used to print a summary of podcasts
downloaded after the terminal is reset. This is useful if you leave
the computer and return after tuxstatus is completed and want to see
the results.

## Issues and Bug Reports

Report and follow issue status at the project's GitLab site.

* ``http://gitlab.com/kookjr/tuxstatus/issues``

# License

GNU GPL, General Public License version 3.0. For details,
see files "COPYING".

# Changelog

v0.3

* add download summary option for after terminal reset/restore
* now recommend official version of tuxcast (since my changes were accepted)
* fix podcasts that have progress that goes 0 to 100 and back to 0%
* cleanup how names are displayed
* tuned the way names are added and removed from various queues

v0.2

* remove entries from Queue marquee line as they are being downloaded

v0.1

* initial version
