/*
 * tuxstatus -- Display status of tuxcast podcast downloads.
 * 
 * Copyright (C) 2009 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of tuxstatus.
 *
 * tuxstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tuxstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tuxstatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REGEX_H
#define REGEX_H

#include <string>
#include <sys/types.h>
#include <regex.h>

#include "RegExException.h"

#define NUM_MATCHES 10

class RegEx {
public:
    RegEx(std::string regex_str);
    RegEx(const RegEx& regex);
    ~RegEx(void);

    RegEx& operator=(const RegEx& regex);

    bool match(std::string str);
    // the whole string matched
    std::string stringMatch(void) throw (RegExException);
    // the substring matched; 1 .. n
    std::string subStringMatch(int which) throw (RegExException);

private:
    void initialize(std::string regex_str) throw (RegExException);

    std::string pattern;
    std::string search_str;
    bool matched_state;
    regex_t regbuf;
    char* errbuf;
    regmatch_t matches[NUM_MATCHES];
};

#endif /* REGEX_H */
/* Local Variables: */
/* mode: C++ */
/* End: */
