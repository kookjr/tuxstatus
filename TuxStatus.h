/*
 * tuxstatus -- Display status of tuxcast podcast downloads.
 * 
 * Copyright (C) 2009, 2010 Mathew Cucuzella (kookjr@gmail.com)
 *
 * This file is part of tuxstatus.
 *
 * tuxstatus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tuxstatus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tuxstatus.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TUXSTATUS_H
#define TUXSTATUS_H

#include <map>
#include <vector>
#include <list>
#include "RegEx.h"

namespace mls { class MultilineStatus; }

class TuxStatus {
public:
    TuxStatus(int delay_ms);
    ~TuxStatus(void);
    void run(void);
    void download_summary_opt(void);

    void proc_progress(RegEx& reg);
    void proc_check(RegEx& reg);
    void proc_download(RegEx& reg);
    void proc_skip(RegEx& reg);
    void proc_queue(RegEx& reg);

private:
    int get_free_line(std::map<std::string,int>& pmap);

    int delay;
    int dl_count;
    bool print_summary;
    std::map<std::string,int> progress_map;
    std::vector<struct line_match_data> line_matches;
    std::list<std::string> download_list;

    mls::MultilineStatus* status;
};

typedef void (TuxStatus::*line_match_action)(RegEx&);

struct line_match_data {
    line_match_data(std::string rex, line_match_action fp) : regex(rex), action(fp) { }
    RegEx regex;
    line_match_action action;
};

#endif /* TUXSTATUS_H */
/* Local Variables: */
/* mode: C++ */
/* End: */
